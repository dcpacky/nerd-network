from django.core.exceptions import ObjectDoesNotExist

from .models import *
from .forms import UserPostForm
from django.shortcuts import render




def get_user_followings(user):
    try:
        obj = UserFollowingWrapper.objects.get(account=user)
    except ObjectDoesNotExist:
        obj = UserFollowingWrapper(
            account=user
        )
        obj.save()
    return obj

def get_posts(request):
    from .encoder import PostEncoder
    from django.core.serializers import serialize
    following = get_user_followings(request.user).following.all()
    #timeline = UserPost.objects.filter(author=request.user)
    timeline = [ obj.as_dict() for obj in UserPost.objects.filter(author=request.user) ]
    #timeline_follow = UserPost.objects.filter(author__in=following)
    timeline_follow = [ obj.as_dict() for obj in UserPost.objects.filter(author__in=following) ]
    for obj in timeline_follow:
        timeline.append(obj)

    timeline = sorted(timeline, key=lambda x: x['id'], reverse=True)
    return timeline

class ViewUtilities:

    def get_user_dashboard(self, request):
        new_post_form = UserPostForm()
        response = render(
            request,
            'dashboard/dashboard.html', {
                'post_form': new_post_form,
            }
        )
        return response

    def get_user_timeline(self, request):
        timeline = get_posts(request)
        return timeline


class TestUtilities:

    @staticmethod
    def generate_post_test_data():
        print("Start Generating Test Data")
        user = get_user_model().objects.get(username='dcpacky')
        from lorem import sentence
        for i in range(0, 1000):
            post = UserPost(
                author=user,
                text=sentence(),
                posted_date=timezone.now(),
            )
            post.save()
        print("Finished Generating Test Data")

    @staticmethod
    def delete_post_test_data():
        print("Deleting Test Data")
        user = get_user_model().objects.get(username='dcpacky')
        UserPost.objects.filter(author=user).delete()
        print("Done Test Data")
