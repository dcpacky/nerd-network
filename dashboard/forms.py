from django import forms
from .models import UserPost


class UserPostForm(forms.ModelForm):
    text = forms.CharField(widget=forms.Textarea(), label='')

    class Meta:
        model = UserPost
        fields = ('text', )
